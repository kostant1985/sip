<?php

namespace SipBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FeedbackType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone', 'text', ['disabled' => true])
            ->add('question', 'textarea', ['required' => false, 'disabled' => true])
            ->add('name', 'text', ['required' => false, 'disabled' => true])
            ->add('email', 'email', ['required' => false])
            ->add('createdAt', 'text', ['required' => false])
            ->add('notes', 'textarea', ['required' => false])
            ->add('status', 'text', ['required' => false])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SipBundle\Entity\Feedback'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sipbundle_feedback';
    }
}

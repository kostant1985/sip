<?php
namespace SipBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('feedback', 'text', [
                'property_path' => 'phone',
                'label' => 'Телефон',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Введите номер'),
                'label_attr' => array('class' => 'col-sm-2 control-label'),
            ])
            ->add('question', 'textarea', [
                'required' => false,
                'label' => 'Опишите Ваш вопрос',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Введите вопрос'),
                'label_attr' => array('class' => 'col-sm-2 control-label'),
            ])
            ->add('name', 'text', [
                'required' => false,
                'label' => 'Ваше имя',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Введите Ваше имя'),
                'label_attr' => array('class' => 'col-sm-2 control-label'),
            ])
            ->add('save', 'submit', [
                'label' => 'Отправить',
                'attr' => ['class' => 'btn btn-primary'],
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SipBundle\Entity\Feedback'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sipbundle_contact';
    }
}
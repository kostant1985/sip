<?php

namespace SipBundle\Controller;

use SipBundle\Entity\Feedback;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use SipBundle\Entity\Task;
use Symfony\Component\HttpFoundation\Request;
use SipBundle\Form\ContactType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="sip_homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('SipBundle:Category')->findAll();


//        ********************************************************

        $array = array(
            array( 'text'    => 'Текст красного цвета',
                'cells'   => '1,2,4,5',
                'align'   => 'center',
                'valign'  => 'center',
                'color'   => 'FF0000',
                'bgcolor' => '0000FF')
        ,array( 'text'    => 'Текст зеленого цвета',
                'cells'   => '8,9',
                'align'   => 'right',
                'valign'  => 'bottom',
                'color'   => '00FF00',
                'bgcolor' => 'FFFFFF')
        );

        $size = array(3,3); //размер таблицы



        $str_tbl = 3; //кол-во строк таблицы
        $cok_str = 3; //кол-во колонок (диапазон строк)
        $num_str = 1; //начало номера строки
        $cell_num=1;//номер исследуемой ячейки

        $row =1;//кол-во объединённых рядов

        $col =1;//кол-во объединенных колонок

        for ($num_str;$num_str<=$str_tbl;$num_str++){//Цикл, который рисует строки таблицы
            //определяем кол-во ячеек, которое надо нарисовать в ряде таблицы
            if ($row = 1){
                $col_rows =$cok_str;
            }else{
                $col_rows=$cok_str-$col;
            }


            $stroka_rez='';

            for ($c=1;$c<=$col_rows;$c++){//цикл, который рисует ячейки в строке
                foreach ($array as $key=>$v) { //Подставляем входные параметры
                    $cells = explode(",", $v['cells']);//массив ячеек
                    if (in_array($cell_num, $cells)){//проверяем есть ли текущая ячейка во входящих ячейках
                        unset($array[$key]);//удаляем массив с найденным значением
                        //исследуем какая форма объединения в 1 строку, в 1 столбик, и комбинаха из 2-х
                        for ($i=1;$i<count ($cells);$i++){
                            $z= $i-1;
                            if (($cells[$i] -$cells[$z])==1){//в 1 ряду
                                        //echo"в 1 ряду";
                                        $col++;

                                    }
                                    elseif(($cells[$i] -$cells[$z])==$cok_str){
                                        //echo"в 1 колонке";
                                        $row++;
                                    }
                                    elseif(($cells[$i] -$cells[$z])==2){
                                       // echo"в квадратной ячейке";
                                        $row++;
                                        $i= $i +$col;//перепрыгиваем ,чтобы попасть на следующую строку исходных ячеек
                                    }
                                }
                        $stroka_rez.= '<td colspan='.$col. ' rowspan='.$row.'></td>';
                        $c=$c+$col;//приращиваем счётчик отрисовки ячеек
                        $cell_num=$cell_num + $col;//счётчик номера следующей ячейки
                    }else{
                        $stroka_rez.='<td></td>';
                        if ($row>1){
                            $cell_num =$cell_num+$col+1;//счётчик номера следующей ячейки
                            --$row;
                        }else{
                            $cell_num++;
                        }

                    }
                }

            }

            //сохраняем в массив код строк
            $stroka_vivod[] = $stroka_rez;

        }







//  **********************************************************



        return $this->render('SipBundle:Default:index.html.twig',  array(
            'categories' => $categories,
        ));
    }

    /**
     * @Route("/prices", name="sip_prices")
     */
    public function PricesAction()
    {
        return $this->render('SipBundle:Default:prices.html.twig');
    }



    /**
     * @Route("/works", name="sip_works")
     */
    public function worksAction()
    {
        return $this->render('SipBundle:Default:works.html.twig');
    }

    /**
     * @Route("/category/{slug}", name="sip_house_category")
     */
    public function categoryAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('SipBundle:Category')->findOneBy(['slug' => $slug]);

        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        return $this->render('SipBundle:Default:category.html.twig', array(
            'category' => $category
        ));
    }

}


<?php
namespace SipBundle\Controller;

use SipBundle\Entity\Feedback;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use SipBundle\Form\ContactType;

/**
 * Contact controller.
 *
 * @Route("/contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("/", name="sip_contact")
     */
    public function newAction(Request $request)
    {
        // создаём задачу и присваиваем ей некоторые начальные данные для примера
        $feedback = new Feedback();


        $form = $this->createForm(new ContactType(), $feedback);

        return $this->render('SipBundle:Contact:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Feedback entity.
     *
     * @Route("/create", name="contact_create")
     * @Method("POST")
     * @Template("SipBundle:Feedback:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Feedback();
        $form = $this->createForm(new ContactType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sip_homepage'));
        }

        return $this->render('SipBundle:Contact:new.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));

    }
}
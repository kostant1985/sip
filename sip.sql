/*
SQLyog Trial v12.12 (64 bit)
MySQL - 5.5.43-0ubuntu0.14.04.1 : Database - sip
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sip` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sip`;

/*Data for the table `Category` */

insert  into `Category`(`id`,`name`,`slug`) values (1,'Дачные дома до 100 м2','do100m2'),(2,'Дома 100 до 150 м2','100-150m2'),(3,'Дома от 150 до 200 м2','150-200m2'),(4,'Дома от 200 до 300 м2','200-300m2');

/*Data for the table `Project` */

insert  into `Project`(`id`,`name`,`slug`,`characteristics`,`price`,`currency`,`floors_quantity`,`area`,`category_id`) values (1,'Рига','riga','','1234567890','грн',10,90,1),(2,'Осло','oslo','','12','копе',10000,100,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

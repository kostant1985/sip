$(document).ready(function(){

        //Прижатие меню к верху страницы при скроллинге
        var $menu = $("#menu");
        $(window).scroll(function(){
            if ( $(this).scrollTop() > 100 && $menu.hasClass("default") ){
                $menu.fadeOut('fast',function(){
                    $(this).removeClass("default")
                           .addClass("navbar-fixed-top")
                           .fadeIn('fast');
                });
            } else if($(this).scrollTop() <= 100 && $menu.hasClass("navbar-fixed-top")) {
                $menu.fadeOut('fast',function(){
                    $(this).removeClass("navbar-fixed-top")
                           .addClass("default")
                           .fadeIn('fast');
                });
            }
        });//Прижатие меню
		
		$(".triggers").animated("zoomInUp", "zoomOutDown");


});//jQuery